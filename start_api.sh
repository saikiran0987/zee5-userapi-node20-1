if [ -z "$1" ]
  then
    echo "No argument supplied"
  else
    npm install &&
    NODE_ENV=$1 npm run start
fi

