const logger = require('../helpers/logger');
const token = require('../../src/helpers/jwtVerifyToken');
const axiosHelper = require('../../src/helpers/axiosCalls')
const auth = require("../helpers/auth");
const UserBackend = require("../helpers/userBackend");
const SIGNINGKEY= process.env.USERAPI_SIGNINGKEY;
const USERSECRET = process.env.USERAPI_USERSECRET;
const USER_API_BASE_URL = process.env.USER_API_BASE_URL;
const COUNTRY_API_URL = process.env.COUNTRY_API_URL;

const _ = require('lodash');
const jwt = require("jsonwebtoken");
const axios = require("axios");
const { sqs } = require("../connections/awsConnection");
const authService = require("../services/authService")
const utf8 = require('utf8');
const crypto = require('crypto');
const fs = require('fs');
const requestIp = require('request-ip');
const nodemailer = require("nodemailer");
const awsSecret = require('../connections/awsSecretManagerSos');
const verifyOTP = require('../services/verifyOTP/verifyOTP')
const redisCluster = require("../connections/redisConnection");
const userService = require("../services/userRegistration/userRegistrationService")
const validatePhoneNumber = require('validate-phone-number-node-js');


module.exports = {
  getDefault: function(reg,res,next){
    res.status(200).send({
      code: 1,
      message: 'hello Default route'
    });
  },


   // Get User Token
   getUserToken: async (req,res,next) => {
    try{
      let email = req.body.hasOwnProperty('email') ? req.body.email : req.query.email;
      let mobile = req.body.hasOwnProperty('mobile') ? req.body.mobile : req.query.mobile;
      let query = '';
      let type = '';
      let body ='';
      let path ='';
      if ((email == null || email ==="" || email.length ===0) &&  (mobile == null || mobile ==="" || mobile.length ===0) ) {
        res.status(403).send({ code: "403", message: "Please provide email or mobile number" });
      }

		if (email) {
      let validEmail = await validateEmail(email);
      if(validEmail === false){
      res.status(403).send({ code: "400", message: "Please provide valid email" });
      }
     	type = "email";
      body = { "email": email};
     	query = `email=${email}`;
			path = `/v1/manage/customer/token?${query}`;
		}else{
      let  validMobileNumber = validatePhoneNumber.validate(mobile);
      if(validMobileNumber === false){
        res.status(403).send({ code: "400", message: "Please provide valid mobile number" });
        }
      type = "mobile";
      body = {"mobile": mobile};
     	query = `mobile=${mobile}`;
			path = `/v1/manage/customer/token?${query}`;
    }

    body =""; 

    let authKey = await createAuthKeys(body,path);
    const custom_headers = {
      'Content-Type': 'application/json',
      'Authorization': authKey
    };

   const response = await axios.get(USER_API_BASE_URL+path,{'headers': custom_headers});
   if(response.status == 200){
        res.status(200).send({
          "code":0,
          "status":true
        });
      }else{
        res.status(404).send({
          "code":1,
          "status":false
        });
      }
    }catch(error){
      if(error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
        res.status(error.response.status).send(error.response.data);
        }
        else {
        res.status(404).send({ code: "1", message: error.message });
        }
    }
  },

    // recreate password login post
  recreatePwdLogin: async (req,res,next) => {
      try{
        let detailsToUpdate ={code:req.body.OTP,new_password:req.body.password}
        let path = "/v1/manage/customer/token";
        let authKey = auth.createAuthKeys(detailsToUpdate,path);
        
        let mobileUrl = (req.body)['type'] == 'mobile'? `/v1/user/recreatepasswordmobile`:`/v1/user/recreatepasswordemail`;

        let apiResponse = await axiosHelper.call("POST",USER_API_BASE_URL+'/v1/user/recreatepasswordmobile',detailsToUpdate,{
          "Content-Type":"application/json-patch+json"
        });

        logger.info(JSON.stringify(apiResponse.data))
        if(apiResponse.status == 200){
            let jwtToken = await token.getJwtToken(req.body);
            apiResponse.data.token = jwtToken.data.token
        }
        res.setHeader('content-type', 'text/html');
        return  res.status(apiResponse.status).send(apiResponse.data)
      }catch(error){
        res.setHeader('content-type', 'text/html');
        logger.error(JSON.stringify(error))
        if(error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
          res.status(error.response.status).send(error.response.data);
        }
        else {
          res.status(404).send({ code: "1", message: error.message });
        }
      }
  },

  b2bSilentRegistration:async(req,res,next)=>{
    try{
      let bodyParams= req.body;
      if(!bodyParams.hasOwnProperty('type') || !bodyParams.hasOwnProperty('value')){
        const error = new Error();
        error.code = 4002;
        error.message = 'Wrong Parameters';
        throw error;
      }
      let type = bodyParams.hasOwnProperty('type')? (bodyParams.type).trim():'';
      let value = bodyParams.hasOwnProperty('value')? (bodyParams.value).trim():'';
      let password = bodyParams.hasOwnProperty('password')? (bodyParams.password).trim():'';
      let partnerkey = bodyParams.hasOwnProperty('partner_key')? (bodyParams.partner_key).trim():'';
      let fname = bodyParams.hasOwnProperty('fname')? (bodyParams.fname).trim():'Guest';
      let lname = bodyParams.hasOwnProperty('lname')? (bodyParams.lname).trim():'User';
      let country = bodyParams.hasOwnProperty('country')? (bodyParams.country).trim():'';
      let additional = bodyParams.hasOwnProperty('additional')? (bodyParams.additional).trim():{};
      additional.partner = partnerkey;
      additional.paymentmode= '';
      additional.recurring_enabled= 'true';
      
      let notification = bodyParams.hasOwnProperty('notification')? (bodyParams.notification).trim():1;
      let userObj = new UserBackend(SIGNINGKEY,USERSECRET,USER_API_BASE_URL);
      let ip = req.connection.remoteAddress;

      if((country).length == 0){
        country = 'IN';
        let countryData = await axiosHelper.call("GET",`${COUNTRY_API_URL}${ip}`);
          if(countryData.data.country_code){
            country= countryData.data.country_code;
          }
      }

      if(password.length < 6){
        const error = new Error();
        error.code = 3;
        error.message = 'The field Password must be a string with a minimum length of 6 and a maximum length of 2147483647';
        throw error;
      }
      
      let userAgent = (req.headers).hasOwnProperty('user-agent')?req.headers['user-agent'] :"API";
      if(!req.query.debug && req.query.debug != 1){
       
            let silentApiresponse =await userObj.silentRegisterUser(value,password,country,ip,type,fname,lname,userAgent,additional);
            if(silentApiresponse.status == 200){
                if(silentApiresponse.data.code){
                  const error = new Error();
                  error.code = silentApiresponse.data.code;
                  error.message = silentApiresponse.data.message;
                  throw error;
                }
            }else{
              if(silentApiresponse.data && silentApiresponse.data.code){
                const error = new Error();
                error.code = silentApiresponse.data.code;
                error.message = silentApiresponse.data.message;
                throw error;
              }else{
                const error = new Error();
                error.code = silentApiresponse.status;
                error.message = silentApiresponse.data;
                throw error;
              }
            }

      }
      let userToken =await userObj.getSilentUserToken(value,type);

      if(userToken.status == "200"){
        if(userToken.data.code){
          const error = new Error();
          error.code = silentApiresponse.data.code;
          error.message = silentApiresponse.data.message;
          throw error;
        }
      }else{
        if(userToken.data && userToken.data.code){
          const error = new Error();
          error.code = silentApiresponse.data.code;
          error.message = silentApiresponse.data.message;
          throw error;
        }else{
          const error = new Error();
          error.code = userToken.status;
          error.message = userToken.data;
          throw error;
        }
      }
      return  res.send({"token":userToken.data.token});
    }catch(error){
      if(error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
        res.status(error.response.status).send(error.response.data);
      }
      else {
        res.status(400).send({ code: error.code, message: error.message });
      }
    }
  },


  // please add your functions here.
  // v2 user login with mobile
  loginMobile: async (req, res, next) => {
    try {
      const bodyData = req.body;
      const headers = req.headers;
      const platform = bodyData.platform;
      headers.host = "whapi.zee5.com"
      // $this->logger->logMessage('INFO',"Attempating to Mobile Login user".md5($decode_json->mobile), LOG_FILE_NAME);
      if ((!bodyData.mobile) || (!bodyData.password) || (!bodyData.platform) || (!bodyData.guest_token)) {
        // $this->logger->logMessage('ERROR',"mobile, password, platform, guest_token are mandatory", LOG_FILE_NAME);
        res.status(400).send({
          "code": 0,
          "message": "mobile, password, platform, guest_token are mandatory"
        });
      }
      else if (platform === "app" && (!bodyData.aid)) {
        // $this->logger->logMessage('ERROR',"Please provide aid", LOG_FILE_NAME);
        res.status(400).send({
          "code": 0,
          "message": "Please provide aid"
        });
      }
      else if (platform && platform.toLowerCase() === 'connected device' && (!bodyData.device)) {
        // $this->logger->logMessage('ERROR',"Please provide device", LOG_FILE_NAME);
        res.status(400).send({
          "code": 0,
          "message": "Please provide device"
        });
      }
      else {
        let result = await axios.post(USER_API_BASE_URL + "/v2/user/loginmobile", bodyData);
        // $this->logger->logMessage('INFO',"loginMobileUser returning...  ".json_encode($response,true),LOG_FILE_NAME);
        let getUserLoggedIn = result.data;
        try {
          if (result.status === 200) {
            // jwt token verification
            const token_details = await authService.verifyToken(getUserLoggedIn.access_token);
            if (token_details) {
              try {
                bodyData.guest_token = await getGuestToken(bodyData);
                const message_body = {
                  "input_param": bodyData,
                  "token_details": token_details,
                  "type": "login_email"
                }
                const param = {
                  'DelaySeconds': 10,
                  'MessageBody': JSON.stringify(message_body),
                  'QueueUrl': 'https://sqs.ap-south-1.amazonaws.com/413362797619/user_enrichment_details_Prod'
                }
                try {
                  const result = await sqs.sendMessage(param);
                  res.status(200).send(getUserLoggedIn)
                } catch (error) {
                  // error handling.
                  logger.error(JSON.stringify(error))
                  // $this->logger->logMessage('ERROR',error.getMessage(),LOG_FILE_NAME);
                }
              } catch (error) {
                logger.error(JSON.stringify(error))
                // $this->logger->logMessage('ERROR',$e->getMessage(),LOG_FILE_NAME);
              }
            }
          } else {
            const token_details = "";
          }
        }
        catch (error) {
          logger.error(JSON.stringify(error))
          // $this->logger->logMessage('ERROR',$e->getMessage(),LOG_FILE_NAME);
        }
      }
    }
    catch (error){
      if(error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
        res.status(error.response.status).send(error.response.data);
      }
      else {
        res.status(404).send({ code: "1", message: error.message });
      }
    }
  },
  loginEmail: async (req, res, next) => {

    try {
      const bodyData = req.body;
      const headers = req.headers;
      // $this->logger->logMessage('INFO',"Attempating to email Login user".md5($decode_json->email), LOG_FILE_NAME);
      if ((!bodyData.email) || (!bodyData.password) || (!bodyData.platform) || (!bodyData.guest_token)) {
        logger.warn("ERROR email, password, platform, guest_token are mandatory")
        // $this->logger->logMessage('ERROR',"email, password, platform, guest_token are mandatory", LOG_FILE_NAME);
        res.status(400).send({
          "code": 0,
          "message": "email, password, platform, guest_token are mandatory"
        });
      }
      else if (bodyData.platform === "app" && (!bodyData.aid)) {
        // $this->logger->logMessage('ERROR',"Please provide aid", LOG_FILE_NAME);
        res.status(400).send({
          "code": 0,
          "message": "Please provide aid"
        });
      }
      else if (bodyData.platform.toLowerCase() === 'connected device' && (!bodyData.device)) {
        // $this->logger->logMessage('ERROR',"Please provide device", LOG_FILE_NAME);
        res.status(400).send({
          "code": 0,
          "message": "Please provide device"
        });
      }
      else {
        let result = await axios.post(USER_API_BASE_URL + "/v2/user/loginemail", bodyData);
        // $this->logger->logMessage('INFO',"loginMobileUser returning...  ".json_encode($response,true),LOG_FILE_NAME);
        let getUserLoggedIn = result.data;
        try {
          if (result.status === 200) {
            // jwt token verification
            const token_details = await authService.verifyToken(getUserLoggedIn.access_token);
            if (token_details) {
              try {
                bodyData.guest_token = await getGuestToken(bodyData);
                const message_body = {
                  "input_param": bodyData,
                  "token_details": token_details,
                  "type": "login_email"
                }
                const param = {
                  'DelaySeconds': 10,
                  'MessageBody': JSON.stringify(message_body),
                  'QueueUrl': 'https://sqs.ap-south-1.amazonaws.com/413362797619/user_enrichment_details_Prod'
                }
                try {
                  const result = await sqs.sendMessage(param);
                  res.status(200).send(getUserLoggedIn)
                } catch (error) {
                  logger.error(JSON.stringify(error))
                  // $this->logger->logMessage('ERROR',error.getMessage(),LOG_FILE_NAME);
                }
              } catch (error) {
                logger.error(JSON.stringify(error))
                // $this->logger->logMessage('ERROR',$e->getMessage(),LOG_FILE_NAME);
              }
            }
          }
          else {
            const token_details = "";
          }
        }
        catch (error) {
          logger.error(JSON.stringify(error))
          // $this->logger->logMessage('ERROR',$e->getMessage(),LOG_FILE_NAME);
        }
      }
    }
    catch (error){
      if(error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
        res.status(error.response.status).send(error.response.data);
      }
      else {
        res.status(404).send({ code: "1", message: error.message });
      }
    }
  },
  changePassword: async (reg,res,next) => {

    try {

      let CUSTOMER_API_URL=USER_API_BASE_URL+'/v1/manage/customer';
      let CUSTOMER_SIGNINGKEY=process.env.USERAPI_SIGNINGKEY;
      let CUSTOMER_USERSECRET=process.env.USERAPI_USERSECRET;

      let tokenDetails = await authService.verifyToken(reg.headers['authorization']);
      let userID = ( tokenDetails && tokenDetails.hasOwnProperty("user_id")) ? tokenDetails['user_id'] : "";
      if( !userID ) {
        res.status(400).send({  message: "Invalid token" });
      } else {

        let passBody = reg.body;
        let finalKey = utf8.encode(CUSTOMER_SIGNINGKEY);
        let userPath = "/v1/manage/customer/"+userID+"/password";
        let fdata = passBody+':'+userPath;
        let finalData = utf8.encode(fdata);
        let hashData = crypto.createHmac('sha256', finalKey).update(finalData);
        let base64EncodedData = hashData.digest('base64');
        let authData = "Z5 "+CUSTOMER_USERSECRET+":"+base64EncodedData;

        const custom_headers = {
          'Content-Type': 'application/json',
          'Authorization': authData
        }
        const response = await axios.put(CUSTOMER_API_URL+'/'+userID+'/password',passBody,{'headers': custom_headers});
        res.setHeader('Content-Type', 'application/json');
        res.status(response.status).send(response.data);
      }

    } catch (err) {
      if(err.hasOwnProperty("response") && err.response.hasOwnProperty("data"))
      {
        res.status(err.response.status).send(err.response.data);
      }
      else
      {
        res.status(404).send({ code: "1", message: err.message });
      }
    }
  },
  getRenewRefreshToken: async (reg,res,next) => {

    try {
      let RENEW_TOKEN_URL=USER_API_BASE_URL+'/v2/user/renew?refresh_token';

      let refreshToken =  reg.headers.hasOwnProperty( 'refresh-token' ) && reg.headers['refresh-token'] ? reg.headers['refresh-token'] : '';
      if( !refreshToken ) {
        return res.status(400).send({ code: "1", message: "Invalid token" });
      } else {
        const response = await axios.post(RENEW_TOKEN_URL+'='+refreshToken);
        res.setHeader('Content-Type', 'application/json');
        res.status(response.status).send(response.data);
      }
    } catch (err) {
      if(err.hasOwnProperty("response") && err.response.hasOwnProperty("data"))
      {
        res.status(err.response.status).send(err.response.data);
      }
      else
      {
        res.status(404).send({ code: "1", message: err.message });
      }
    }
  },
  silentRegistration: async (reg,res,next) => {

    try {

      let COUNTRY_API_URL=process.env.COUNTRY_API_URL;
      let B2B_SILENT_REG_URL='https://whapi.zee5.com/v1/user/b2bSilentRegistration.php';
      let REG_GET_CODE_URL='https://useraction.zee5.com/device/v2/getcode.php';
      let EMIL_CONFIRM_URL='https://www.zee5.com/?web=true&tag=';
      let SMPT_SECRET_NAME='prd-whapi_smtp';
      //find ip of user from HTTP_TRUE_CLIENT_IP
      let clientIp = requestIp.getClientIp(reg);

      param_arr = {};
      param_arr['type'] = "email";
      param_arr['value'] = reg.body.email;
      param_arr['fname'] = reg.body.first_name ? reg.body.first_name:"Guest";
      param_arr['lname'] = reg.body.last_name ? reg.body.last_name:"Guest";
      param_arr['partner_key'] = "zee5";
      param_arr['password'] = reg.body.password;
      if(reg.body.additional) {
        param_arr['additional'] = reg.body.additional;
      }

      let regdata = param_arr;
      //once you have the IP send it to another api to get country of the user.
      let country = 'IN';
      let country_res = await axios.get(COUNTRY_API_URL+clientIp);
      if(country_res.hasOwnProperty("data")) {
        country = country_res['data']['country_code'];
      }
      regdata['registration_country'] = country;
      let passBody = JSON.stringify(regdata);
      //let passBody = regdata;

      const custom_headers = {
        'Content-Type': 'application/json',
      }
      const regresponse = await axios.post(B2B_SILENT_REG_URL,passBody,{headers: custom_headers});
      let userToken = regresponse.data.token;

      requestfields = {};
      requestfields['identifier'] = "web";
      requestfields['partner'] = "contactus";
      requestfields['authorization'] = "bearer "+userToken;
      let passBodyReq = JSON.stringify(requestfields);

      const tokenresponse = await axios.post(REG_GET_CODE_URL,passBodyReq,{headers: custom_headers});
      let emilurl= EMIL_CONFIRM_URL+tokenresponse.data.token;

      let emailres = await sendEmail(param_arr['value'],emilurl,"Verify your email address on ZEE5");
      if(emailres) {
        res.setHeader('Content-Type', 'application/json');
        res.status(200).send({"code": 1,"message": "Registration successful"});
      }
    } catch (err) {
      if(err.hasOwnProperty("response") && err.response.hasOwnProperty("data"))
      {
        return res.status(err.response.status).send(err.response.data);
      }
      else
      {
        return res.status(404).send({ code: "1", message: err.message });
      }
    }

  },
  getUserLogin: async (req,res,next) => {
    try {
      let SIGNINGKEY = process.env.USERAPI_SIGNINGKEY
      let USERSECRET = process.env.USERAPI_USERSECRET
      let token = await authService.verifyToken(req.headers['authorization'])
      let userID = ( token && token.hasOwnProperty("user_id")) ? token['user_id'] : "";
      if( !userID ) {
        return res.status(400).send({  message: "Invalid token" });
      } else {
        let body = "";
        let path = `/v1/manage/customer/${userID}`;
        let finalKey = utf8.encode(SIGNINGKEY);  //utf8 encode the key
        let data = `${body}:${path}`; // data to encode is body : and path as shown below
        let finalData = utf8.encode(data);
        let hashedData = crypto.createHmac('sha256', finalKey).update(finalData);
        let base64EncodedData = hashedData.digest('base64');
        let authKey = `Z5 ${USERSECRET}:${base64EncodedData}`; //final auth format
        let fullPath = `${USER_API_BASE_URL}${path}`;
        const response = await axios.get(fullPath,{headers: {
          'Content-Type': 'application/json',
          'Authorization': authKey
        }});
        res.setHeader('Content-Type', 'application/json');
        res.status(response.status).send(response.data);
      }
    } catch (err) {
      if(err.hasOwnProperty("response") && err.response.hasOwnProperty("data")) {
        res.status(err.response.status).send(err.response.data);
      }
      else {
        res.status(404).send({ code: "1", message: err.message });
      }
    }
  },
  // New user Registration through email or mobile no.
  userRegistration: async (req, res, next) => {

    if (req.body && req.body.additional) {
      Object.keys(req.body.additional).forEach(key => {
        req.body.additional[key] = ('' + req.body.additional[key]).replace(" ", "+")
      })
    }

    if (req.body && req.body.type && req.body.value) {
      let user = {
        type: req.body.type,
        value: req.body.value,
        fname: req.body.first_name && req.body.first_name.trim() == '' ? req.body.first_name : "Guest",
        lname: req.body.last_name && req.body.last_name.trim() !== '' ? req.body.last_name : "Guest",
        password: req.body.password,
        partner_key: req.body.partner_key && req.body.partner_key !== '' ? req.body.partner_key : "zee5"
      }

      if (req.body.additional) {
        user.additional = req.body.additional;
      }
      // httpVersionMajor: 1,
      // httpVersionMinor: 1,
      // httpVersion: '1.1',
      let version = '';
      if(req.query.hasOwnProperty("version"))
        version = req.query.version;

      let country = await userService.getRegistrationCountryCode(req._remoteAddress);
      if (user.type == 'mobile') {
        let mobileno = user.value;
        user.registration_country = country;
        let userExists = await userService.checkuserExist(mobileno);
        logger.info("userExists  = "+JSON.stringify(userExists))
        if (!userExists.status) {
          // if (true) {
          let otp = userService.rand(1000, 9999); /*generate random OTP code*/
          let newOTP = await userService.checkPasswordOTPInRedis('PWD_OTP:' + mobileno, otp);/*check if same number exists in Redis in OTP record*/
          if (newOTP) {
            let otpsent = await  userService.sendOTP(mobileno, otp, version);
            if(otpsent.code == 1){

              logger.info("otpsend status");
              logger.info(JSON.stringify(otpsent));
              userService.createTempRecordInRedis('TEMP_RECORD:' + mobileno, 'OTP', otp);
              userService.createTempRecordInRedis('TEMP_RECORD:' + mobileno, 'DATA', JSON.stringify(user));
              userService.createPasswordRecordInRedis('PWD_OTP:' + mobileno, otp);
              res.status(200).send(otpsent)
            }else{
              res.status(404).send(otpsent)
            }

          }

        } else {
          res.status(400).send(userExists.data)
        }

      } else if (user.type == "email") {
        user['country'] = country;

        let silentRegister = await userService.loginwithB2B(user);
        let result = {
          status: false,
          data: null
        }
        if (silentRegister.status) {
          result.status = silentRegister.status;
          result.data = silentRegister.data;
        } else if (silentRegister.response.status) {
          result.status = silentRegister.response.status;
          result.data = silentRegister.response.data;
        }
        logger.info(JSON.stringify(result))
        res.status(result.status).send(result.data);
      }
    } else {

      if(req.body.type=='' && req.body.value == ''){
        res.status(400).send({
          "code": "404",
          "message": "Please provide the email"
        })
      }
      else if(req.body.type && (!req.body.value || req.body.value == '')){
        if (req.body.type == 'email' ) {
          res.status(400).send({
            "code": "404",
            "message": "Please provide the email"
          })
        } else if (req.body.type == 'mobile' ) {
          res.status(400).send({
            "code": "404",
            "message": "Please provide the mobile number"
          })
        } else {
          res.status(400).send({
            "code": "404",
            "message": "Please provide the email"
          })
        }
      }else if(req.body.value && (!req.body.type || req.body.type == '')){
        res.status(400).send({
          "code": 1,
          "message": "One of email or mobile is required"
        });
      }

    }
  },
  updateUserDetails: async (req, res, next) => {
    try{
      let json = req.body;
      if( typeof(req.body) == "string"){
        json = JSON.parse(req.body);
      }
      if((_.isEmpty(json.platform)) || (_.isEmpty(json.guest_token))){
        return res.status(400).send({message: "platform, guest_token are mandatory."})
      }
      if(json.platform.toLowerCase() =="app" && _.isEmpty(json.aid)){
        return res.status(400).send({message: 'Please provide aid'})
      }
      if (json.platform.toLowerCase() == 'connected device' && _.isEmpty(json.device)){
        return res.status(400).send({message: 'Please provide device'})
      }

      authHeader = !_.isEmpty(req.header('Authorization')) ? req.header('Authorization') : req.header('authorization');
      let tokenValue =  authHeader.split(" ");
      tokenDetails = await authService.verifyToken(tokenValue[1]);

      if (!_.isEmpty(tokenDetails) && tokenDetails != false){
        await markEmailMobileVerified(json, tokenDetails);
        let userUpdateResponse = await userUpdate(json, authHeader);
        let httpCode = userUpdateResponse.status;
        let getUserLoggedIn = userUpdateResponse.data;

        if (httpCode == 200){
          if (json.hasOwnProperty('birthday') && !_.isEmpty(json.birthday)){
            date = json.birthday;
            date = date.slice(0, 10).split(); //need to check
            let dateCreate = new Date(date[0]);
            let today = new Date();
            tokenDetails.age = Math.floor(((today - dateCreate) / (1000*60*60*24))/365);
          }
          if(json.hasOwnProperty('gender') && !_.isEmpty(json.gender)) {
            tokenDetails.gender = json.gender;
          }
          if (json.hasOwnProperty('guest_token') && !_.isEmpty(json.guest_token)){
            await SQSOperations(json,tokenDetails);
            res.status(200).send(getUserLoggedIn);
          }
        }
      }else{
        return res.status(err.status).send(err.data);
      }
    }catch(error){
      if(error.hasOwnProperty("response") && error.response.hasOwnProperty("data")){
        res.status(error.response.status).send(error.response.data);
      }else{
        res.status(404).send({ code: "1", message: error.message });
      }
    }
  },

  verifyOTP: async (req,res,next) => {

    try {
      let reqData = req.body;
      if(reqData.hasOwnProperty("otp") && reqData.otp !=="" && reqData.hasOwnProperty("mobile") && reqData.mobile !=="") {
        let redishHash = 'TEMP_RECORD:'+reqData.mobile;
        let redishHashKey= 'OTP';
        let responseFromRedis = await redisCluster.hget(redishHash, redishHashKey);
        if(req.query.test){
          return res.send(responseFromRedis);
        }
        if(responseFromRedis == reqData.otp){
            const silentRegister =  await verifyOTP.doSilentRegistration(reqData.mobile);
            //if(silentRegister && silentRegister.status && silentRegister.status !== 200){
              //return res.status(400).send(silentRegister.data);
            //}

            let refreshToken = await verifyOTP.getRefreshToken(silentRegister.data.token);
            silentRegister.data.refresh_token = refreshToken.data.refresh_token;
            silentRegister.data.expires_in = refreshToken.data.expires_in;
            silentRegister.data.token_type = refreshToken.data.token_type;

            redisCluster.del("TEMP_RECORD:"+reqData.mobile);

            let statuscode = silentRegister && silentRegister.status ? silentRegister.status : 200
            res.status(statuscode).send(silentRegister.data)
      }else{
          res.setHeader('content-type', 'text/html');
          res.status(404).send({"code": "404","message": "Invalid OTP"})
        }
      } else {
        res.setHeader('content-type', 'text/html');
        res.status(404).send({"code": "404","message": "Please provide OTP & mobile Number"})
      }
    }
    catch(error){
      if(error.hasOwnProperty("response") && error.response.hasOwnProperty("data")){
        res.status(error.response.status).send(error.response.data);
      }else{
        res.status(404).send({ code: "1", message: error.message });
      }
    }
  },

}

validateEmail = async (email) => {
       let emailRegex = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
       try {
       if (!email)
        return false;

      if(email.length>254)
        return false;

      var valid = emailRegex.test(email);
      if(!valid)
        return false;

      // Further checking of some things regex can't handle
      var parts = email.split("@");
      if(parts[0].length>64)
        return false;

      var domainParts = parts[1].split(".");
      if(domainParts.some(function(part) { return part.length>63; }))
        return false;
       }catch (err) {
        logger.error(JSON.stringify(err))
       }

  }



createAuthKeys = async (body,path) => {
  try {
      let signInKey=process.env.USERAPI_SIGNINGKEY;
      let userSecrate=process.env.USERAPI_USERSECRET;
      
      let finalKey = utf8.encode(signInKey); //utf8 encode the key
      let data = `${body}:${path}`;  // data to encode is body : and path as shown below
      let finalData = utf8.encode(data);
      let hashData = crypto.createHmac('sha256', finalKey).update(finalData);
      let base64EncodedData = hashData.digest('base64');
      let retult = `Z5 ${userSecrate}:${base64EncodedData}`; //final auth format
     return retult;
   } catch (err) {
    logger.error(JSON.stringify(err))
   }  
  
  }

getGuestToken = async (bodyData) =>{
  if (bodyData.platform == "app") {
    let api_data = {
      "user": {
        "apikey": "6BAE650FFC9A3CAA61CE54D",
        "aid": bodyData.aid
      }
    };
    let result = await axios.post("https://useraction.zee5.com/user/",
        api_data,
        {
          headers: postHeaders
        });
    return result.data.guest_user;
  }else{
    //pass whatever is there in query params
    return bodyData.guest_token;
  }
}
sendEmail  =  async (to,emailurl,subject) => {
  try {

    let template = '<table style="background-color: #fff; max-width: 600px; margin: auto;" border="0" cellspacing="0" cellpadding="0" align="center" data-ogsb="white"><tbody><tr><td align="left" valign="top"><a style="color: #e49fff !important;" title="" href="http://bit.ly/2FynsOw" target="_blank" rel="noopener noreferrer" data-auth="VerificationFailed" data-ogsc=""><img style="display: block; width: 88px; height: 87px; padding-top: 15px; border-width: 0;" title="ZEE5Logo" src="https://useraction.zee5.com/userimages/zeelogo.png" alt="ZEE5Logo" data-imagetype="External" /></a></td></tr><tr><td style="font-family: Arial,Helvetica,sans-serif;" align="left"> <table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td>&nbsp;</td></tr><tr><td><h2 style="font-size: 16px;">Hi,</h2><h3 style="font-size: 16px;">Welcome to ZEE5!<br /> <br /> You\'re just one step away from setting up your account.</h3><p style="font-size: 14px; margin-top: 0; margin-bottom: 0;">Please click here to confirm your email.</p><br /> <a style="color: white; font-size: 15px; background-color: #bc0069; text-decoration: none; border-radius: 5px; padding: 13px 40px; border-width: 2px;" title="" href="'+emailurl+'" target="_blank" rel="noopener noreferrer" data-auth="VerificationFailed">Verify Email </a></td></tr><tr><td>&nbsp;</td></tr><tr><td><p style="font-size: 14px; margin-top: 0; margin-bottom: 0;">If incase the above button, doesn\'t work, please copy &amp; paste the below link in your browser, <br /> <a style="color: #ff83c7 !important; font-size: 14px; text-decoration: none;" title="" href="'+emailurl+'" target="_blank" rel="noopener noreferrer" data-auth="VerificationFailed" data-ogsc="rgb(188, 0, 105)">'+emailurl+'</a></p></td></tr><tr><td><p style="font-size: 12px; margin-top: 0; margin-bottom: 0;">You are getting this email as a registered user of ZEE5.<br /> This email is auto-generated so please do not reply to this email as we will be unable to respond from this email address. Please connect with us on <span><a style="color: #ff83c7 !important; font-size: 12px; text-decoration: none;" href="mailto:support.in@zee5.com" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" data-ogsc="rgb(188, 0, 105)">support.in@zee5.com</a> </span>for any concerns.<br /> <br /> This email address will be used to share periodic updates regarding new TV Shows, Movies, Originals that are available on ZEE5. </p></td></tr><tr><td>&nbsp;</td></tr><tr><td style="background-color: #000;" width="100%" height="1"></td></tr><tr><td><p style="color:#808080;font-size: 12px;" data-ogsc="gray">&copy; 2018 ZEE5</p></td></tr><tr><td style="height: 45px;" align="center" valign="middle">&nbsp;</td></tr><tr><td>&nbsp;</td></tr></tbody></table></td></tr></tbody></table></p>';

    //let secretData = await awsSecret(SMPT_SECRET_NAME);

    let transporter = nodemailer.createTransport({
      host: "smtp.pepipost.com",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'zee5com', // generated ethereal user
        pass: '91d0!7baf38ab', // generated ethereal password
      },
    });
    let info = await transporter.sendMail({
      from: '"Zee5 " <noreply@zee5.com>', // sender address
      to: to, // list of receivers
      subject: subject, // Subject line
      html: template, // html body
    });
    return true;

  } catch (error) {
    return {
      error:error
    }
  }
};
markEmailMobileVerified = async (decodeJson, headers) => {
  env = "prod"
  type = '';
  verifyJson = {};
  if (decodeJson.hasOwnProperty('email') && decodeJson.hasOwnProperty('mobile')) {
    type = 'both';
  }else if (decodeJson.hasOwnProperty('email')) {
    type = 'email';
  }else if (decodeJson.hasOwnProperty('mobile')) {
    type = 'mobile';
  }else {
    type = '';
  }

  if(!_.isEmpty(headers) && headers != false){
    authKey = process.env.CMS_AUTH_KEY;
    result1 =  await axios.get(
        `http://13.232.167.32:80/perl.php?id=${headers.user_id}&type=${type}&env=${env}`,
        {
          headers: {
            "Content-Type": "application/json-patch+json",
            "Authorization": authKey
          }
        });
    return result1;
  }
}
userUpdate = async (decodeJson, authToken) => {
  let tokenValue =  authToken.split(" ");
  tokenDetails = await authService.verifyToken(tokenValue[1]);
  decodeJson.id = tokenDetails.user_id;
  authKey = 'Cms '+process.env.CMS_AUTH_KEY;
  const response = await axios.put(USER_API_BASE_URL+'/v1/manage/customer', decodeJson,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authKey
        }
      });

  let httpCode = response.status;
  if(httpCode == 200){
    result2 = await axios.post(USER_API_BASE_URL+'/v2/user/tokenexchange', '', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': authToken
      }
    });

    if (result2.data.hasOwnProperty('refresh_token')){
      response.data.refresh_token = result2.data.refresh_token;
    }
    return response;
  }
}
SQSOperations = async (decodeJson,tokenDetails) => {
  const messageBody = {
    "input_param": decodeJson,
    "token_details": tokenDetails,
    "type": "update_user"
  }
  const param = {
    'DelaySeconds': 10,
    'MessageBody': JSON.stringify(messageBody),
    'QueueUrl': 'https://sqs.ap-south-1.amazonaws.com/413362797619/user_enrichment_details_Prod'
  }
  try {
    const result = await sqs.sendMessage(param);
    return result
  } catch (error) {
    return error
  }
}
