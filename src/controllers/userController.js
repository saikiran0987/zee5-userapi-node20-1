const bcryptService = require('../helpers/bcrypt');
const jwtService = require('../services/authService');
const {userState} = require('../config/enums')
const response = require('../helpers/responseHelper');
const userServices =  require('../services/user/userService');
//const userId = 'D773F817-C24C-4113-B3AE-0A2FFC0E37CC';
const redisCluster =  require('../connections/redisConnection');
const { object } = require('../connections/redisConnection');
const redisKey = 'user_';
const otpService = require('../services/others/otpService');
const authService = require("../services/authService");

const logger = require('../helpers/logger');
const { isNull } = require('lodash');
const Reader = require('@maxmind/geoip2-node').Reader;
const passwordServices = require('../services/password/passwordService');
const _ = require('lodash');

module.exports = {
// Get User
getUser : async (req, res, next) => { 
    try {
      let result = await redisCluster.get(`${redisKey}${req.headers.userid}`);
      if (!result || result.length == 0) {
          dbData = await userServices.getUser(req, res);
          result = JSON.stringify(dbData);
          await redisCluster.set(`${redisKey}${req.headers.userid}`, result);
      }
      result =JSON.parse(result);
      result.IsPasswordAttached = !result.PasswordHash.endsWith("_user");

      if(result.PasswordHash) {
        delete result.PasswordHash;
      }

      result = responseMapper(result);
      res.status(200).send(result);
    }catch(err){
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
    }
},



LoginEmailV2: async( req,res)=>{
  try{
    let tokenValue = null;
    let email = req.query.email;
    let password = req.query.password;
    //email, password validation and user state validation : ideal user state :confirmed 
    const userWithEmail = await userServices.validateUserWithEmail(email);
    //passwordHash error handling
    if(userWithEmail.PasswordHash=="")
      throw response.generate401(2122,"User must login with specified account.")
    if(userWithEmail.State!=userState.Verified)
      throw response.generate401(2121,"The email address of the user is not confirmed")
    const passwordAuthorizationResult = await bcryptService.verifyPasswordHash(password,userWithEmail.PasswordHash)
    if(passwordAuthorizationResult==='true'){ 
      await userServices.setLastLogin(userWithEmail)
      tokenValue = await jwtService.generateAccessToken(userWithEmail)    
    }
    if(passwordAuthorizationResult==='false')
      throw response.generate401(2120,"The email address and password combination was wrong during login")  
    let result={
      token:tokenValue
    }
    //const result = token
    if(!isNull(result.token)){
      res.status(200).send(result);
    }
    else{
      let apiResponse = response.generate(2063, 'The confirmation code was not found in our system',404, result)
      res.status(404).send(apiResponse)
    } 
  }catch(err){
    logger.error(JSON.stringify(err))
    res.statusCode= 401;
    res.send(err) 
  }
},


updateUser : async (req, res, next) => {
    try {

      }catch(err){
        logger.error(JSON.stringify(err))
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
    }
},


deleteUser : async (req, res, next) => {
    try {
        const result = await userServices.deleteUser(req, res);
        //await redisCluster.del(`${redisKey}${req.headers.userid}`);
        // let apiResponse = response.generate(1, "Delete successful");
        if (result) {
            res.status(200).send({
                "code":1,
                "message":"Delete successful"
              });
        }
      }catch(err){
        logger.error(JSON.stringify(err))
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
    }
},

UpdateUserV2 : async (req, res, next) => {
    try {

      }catch(err){
        logger.error(JSON.stringify(err))
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
    }
},

//confirmmobile is for validating user OTP which is received on Mobile
confirmMobile : async (req, res, next) => {
  try {
    const result = await userServices.confirmMobile(req,res);
    if(result){
      let apiResponse = response.generate(1, 'Confirmation successful',200, result)
      res.status(200).send(apiResponse);
    }
    else{
      let apiResponse = response.generate(2063, 'The confirmation code was not found in our system',404, result)
      res.status(404).send(apiResponse)
    }
  }catch(err){
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
},

confirmemailv2 : async (req, res, next) => {
  try {
    const result = await userServices.confirmemailv2(req,res);
    //await redisCluster.del(`${redisKey}${req.headers.userid}`);
    if(result){
      let apiResponse = response.generate(1, 'Confirmation successful',200, result)
      res.status(200).send(apiResponse);
    }
    else{
      let apiResponse = response.generate(2063, 'The confirmation code was not found in our system',404, result)
      res.status(404).send(apiResponse)
    }
  }catch(err){
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
 },

 //changepassword 
changePassword : async (req, res, next) => {
  try{

    //check for required req-body params
    if (!"new_password" in req.body || !("old_password" in req.body)){
      return res.status(400).send({code: 3, message: "Invalid input parameter."});
    }
    let decodedToken = await authService.verifyToken(req.headers['authorization'])
    
    //uncomment this while pushing to stage
    let userID = ( decodedToken && decodedToken.hasOwnProperty("user_id")) ? decodedToken['user_id'] : "";

    if( !userID ) {
      return res.status(401).send(decodedToken);
    }

    // for testing
    // let userID = "FD1228DE-EF4C-4381-B7D3-00001D3BB1B3";
    // await redisCluster.del(`${redisKey}${userID}`)
    
    let redisResult  = await redisCluster.get(`${redisKey}${userID}`);    

    if (redisResult || redisResult != null) {

      redisResult =JSON.parse(redisResult);
      return await verifyAndUpdatePassword(req,redisResult,res,next);

    } else{

      //fetch record from dynamo
      let userRecord = await userServices.getUserFromId(userID);
      logger.info("userRecord => ",JSON.stringify(userRecord))

      if (userRecord.hasOwnProperty("Item") && !_.isEmpty(userRecord) && !_.isEmpty(userRecord.Item)) {
        return await verifyAndUpdatePassword(req,userRecord.Item,res,next);
      } else {
        //find in mssql afterwards; currently return with user not found.
        return res.status(404).send({code: 2, message: "The user could not be found."});
      }
    }
  } catch(error){
    logger.error(JSON.stringify(error))
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
},
};


responseMapper = (data) => {
  formattedData = [data].map(({ Id, System, Email, EmailVerified, Mobile, MobileVerified, FirstName, LastName, MacAddress,
    Birthday, Gender, ActivationDate, Activated, IpAddress, RegistrationCountry, RegistrationRegion, Json, IsPasswordAttached }) => ({
      "id": Id,
      "system": System,
      "email": Email,
      "email_verified": EmailVerified,
      "mobile": Mobile,
      "mobile_verified": MobileVerified,
      "first_name": FirstName,
      "last_name": LastName,
      "mac_address": MacAddress,
      "birthday": Birthday,
      "gender": Gender,
      "activation_date": ActivationDate,
      "activated": Activated,
      "ip_address": IpAddress,
      "registration_country": RegistrationCountry,
      "registration_region": RegistrationRegion,
      "additional": Json,
      "is_password_attached": IsPasswordAttached

    }));

  return formattedData[0];

};

async function verifyAndUpdatePassword(req,userRecord,res,next) {

  let ipAddress = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
  let reader = await Reader.open(process.cwd()+'/src/GeoIP/GeoIP2-City.mmdb');
  let geoResponse = await reader.city(ipAddress);
  let countryCode = geoResponse.country.isoCode; // 'eg: IN'

  let isVerifiedPassword = await passwordServices.verifyPassword(req.body.old_password, userRecord.PasswordHash);

  if(!isVerifiedPassword){
    return res.status(400).send({code: 2103, message: "The old password was not correct."});
  }

  let newPwdHash = await passwordServices.hashPassword(req.body.new_password, userRecord.PasswordHash);
  let updatedResult = await userServices.updatePassword(userRecord.Id,newPwdHash);
  logger.info(`Password changed of ${userRecord.Id}: updatedResult => `, updatedResult);

  if(updatedResult){
    //insert in userProfileUpdateHistory also
    var insertItem = {
      UserId: userRecord.Id,
      EmailId: userRecord.Email,
      MobileNumber: userRecord.Mobile,
      IpAddress: ipAddress,
      CountryCode: countryCode,
      RequestPayload: await passwordServices.hidepassword(req.body),
      PasswordUpdated: true
    };
    let insertItemResult = await userServices.insertUserProfileUpdateHistory(insertItem);
    if(insertItemResult){
      logger.info(`Record Inserted in UserProfileUpdateHistory table insertItem => `, insertItem);
      //set key in redis and return response
      userRecord.PasswordHash = newPwdHash
      logger.info(`Redis data set for : ${redisKey}${userRecord.Id}`, userRecord);
      await redisCluster.set(`${redisKey}${userRecord.Id}`, JSON.stringify(userRecord));
      return res.status(200).send({code: 1, message: "Password was changed successfully"});
    } else {
      return res.status(500).send({code: 2909, message: "Item couldn't be added (DB insert failure)"});
    }
  }
  else{
    return res.status(500).send({code: 2104, message: "Password couldn't be updated (DB update failure)"});
  }
};
