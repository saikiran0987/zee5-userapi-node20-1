const businessLogic = require('../services/reminders/remindersService');
const response = require('../helpers/responseHelper');
const redisCluster = require('../connections/redisConnection');
const config = require('../config/config');


// Get the favorites of the current user
exports.getReminders = async (req, res, next) => {
    try {

        let result = await redisCluster.get(`${config.reminders.redis_prefix}${req.headers.userid}`);
        if (!result) {
            const resultAll = await businessLogic.getReminders(req, res);
            result = JSON.stringify(resultAll);
            await redisCluster.set(`${config.reminders.redis_prefix}${req.headers.userid}`, result);
        }
        result = dataProjection(result);

        //let apiResponse = response.generate(200, 'Data found', result);
        let apiResponse = result;
        res.status(200).send(apiResponse);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}


// Add an item to the favorites of the current user
exports.addReminders = async (req, res, next) => {
    try {
        const result = await businessLogic.addReminders(req, res);
        await redisCluster.del(`${config.reminders.redis_prefix}${req.headers.userid}`);
        if (result) {
            //let apiResponse = response.generate(1, 'reminders was added successfully');
            let apiResponse = {
                "code":1,
                "message":"reminders was added successfully"
            }
            res.status(200).send(apiResponse);
        }     
    } catch (err) {
        if (!err.statusCode) {
            err.message = "Item couldn't be added (DB insert failure)";
            err.statusCode = 500;
        }
        next(err);
    }
}


// Update an item in the favorites of the current user
exports.updateReminders = async (req, res, next) => {
    try {
        const result = await businessLogic.updateReminders(req, res);
        await redisCluster.del(`${config.reminders.redis_prefix}${req.headers.userid}`);
        if (result) {
        //let apiResponse = response.generate(1, 'reminders was updated successfully');
        let apiResponse = {
            "code":1,
            "message":"reminders was updated successfully"
        }
        res.status(200).send(apiResponse)
        }
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}


// Delete an item from the favorites of the current user
exports.deleteReminders = async (req, res, next) => {
    try {
        const result = await businessLogic.deleteReminders(req, res);
        await redisCluster.del(`${config.reminders.redis_prefix}${req.headers.userid}`);
        //let apiResponse = response.generate(1, "Delete successful")
        let apiResponse = {
            "code":1,
            "message":"Delete successful"
        }
        if (result) {
            res.status(200).send(apiResponse)
        }

    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}


// Format the data as per response needed
dataProjection = (data) => {
    data = JSON.parse(data);
    data = data.Items.map(({ AssetId, AssetType, reminderType }) => ({
        id: AssetId,
        asset_type: AssetType,
        reminder_type: reminderType
    }));
    return data;
};
