const redisCluster = require("../../connections/redisConnection");
const axios = require('axios');

exports.getRefreshToken = async (token) => {
    const options = {
        headers: {
            "Content-Type": "application/json",
            "Authorization": "bearer " + token
        }
    }
    return await axios.post("http://user-acc.zee5.com/v2/user/tokenexchange", "", options).then(res => {
        return res;
    }).catch(err => {
        return err.message
    })

}

exports.doSilentRegistration = async (mobileno) => {
    let redishHash = 'TEMP_RECORD:' + mobileno;
    let redishHashKey = 'DATA';
    let userData = await redisCluster.hget(redishHash, redishHashKey);
    try {
        return axios.post('https://stagingb2bapi.zee5.com/partner/api/silentregister.php', userData)
            .then(res => {
                return res;
            }).catch(err => {
                return err.response;
            });
    } catch (err) {
        return {
            status: false,
            code: 500,
            data: err.message
        }
    }

}
