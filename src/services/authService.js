const config = require('../config/config')
const secretToken = config.TokenServiceOptions.TOKEN_SECRET;
const expiresIn = config.TokenServiceOptions.AccessTokenLifeTime;
const jwt = require("jsonwebtoken");
const fs = require('fs');

class authService{

    static async verifyToken(token) {
        try {
            if(!token){
                return{
                    code: 401,
                    message: "Authorization failed",
                    fields: [
                        {
                            field: "JwtAuthProvider",
                            message: "Authentication header is required"
                        },
                        {   field: "OAuthJwtAuthProvider",
                            message: "Invalid authentication token"
                        }
                    ]
                };
            }
            if (token.indexOf("bearer") != -1) {
                const NumberOfIndex = token.indexOf("bearer");
                token = token.slice(NumberOfIndex + 7);
            }
            const cert = fs.readFileSync(process.cwd()+'/src/config/rsa_prod.pem');
            try{
                let decoded;
                decoded = await jwt.verify(token,cert);
                return decoded;
            } catch(e){
                console.log(e.message)
                return {
                    code: 401,
                    message: "Authorization failed",
                    fields: [
                        {
                            field: "JwtAuthProvider",
                            message: "Invalid authentication token"
                        },
                        {
                            field: "OAuthJwtAuthProvider",
                            message: e.message
                        }
                    ]
                }
            }
        } catch (error) {
            return {
                code: 401,
                message: "Authorization failed",
                fields: [
                    {
                        field: "JwtAuthProvider",
                        message: "Invalid authentication token"
                    },
                    {
                        field: "OAuthJwtAuthProvider",
                        message: error.message
                    }
                ]
            }
        }
    }

    //generation of client token based on user
    static async generateAccessToken(user) {
        return jwt.sign({data:user}, secretToken, { expiresIn: expiresIn });
    }

    //authentication while logging in (non-wrapper) 
    static async authenticateToken(req, res, next){
        const authHeader = req.headers['authorization']
        const token = authHeader && authHeader.split(' ')[1]
        jwt.verify(token, secretToken, (err, user) => {
            if (err) 
                return false
            req.user = user
            next()
        })
    }
    static async refreshToken(req,res,next){
          
    }
}

module.exports = authService
    