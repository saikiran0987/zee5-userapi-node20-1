const UUID = require('uuid');
const dynamoDB = require('../../connections/dynamoConnection');
const config = require('../../config/config');
const remindersValidation = require("./remindersValidation");
const tableName = config.reminders.table;
const indexName = config.reminders.index
const logger = require('../../helpers/logger')
// TODO: get UserId from Auth token

exports.addReminders = async (req, res) => {
    await remindersValidation.validateInputs(req, res);
    await remindersValidation.validateAssetType(req, res);
    let data = await isItemExists(req);
    if (data.Items.length > 0) {
        let error = new Error();
        error = { message: "Item Already exists", statusCode: 2411, status: 404 }
        logger.error(JSON.stringify(error));
    }
    const params = {
        TableName: tableName,
        Item: {
            Id: UUID.v4(),
            userId: req.headers.userid,
            AssetId: req.body.id,
            AssetType: req.body.asset_type,
            reminderType: req.body.reminder_type,
            createdAt: Date.now()
        },
    };
    result = await dynamoDB.addDynamo(params);
    return result;
}

exports.getReminders = async (req, res) => {

    const params = {
        TableName: tableName,
        IndexName: 'user_index',
        KeyConditionExpression: 'userId = :userId',
        ExpressionAttributeValues: {
            ':userId': req.headers.userid
        },
    };

    result = await dynamoDB.getDynamo(params);
    return result;
}


exports.updateReminders = async (req, res) => {

    await remindersValidation.validateAssetType(req, res);
    let data = await isItemExists(req);
    if (data.Items.length == 0) {
        let error = new Error();
        error = { message: "Item couldn't be found", statusCode: 2, status: 404 }
        logger.error(JSON.stringify(error));
    }

    const params = {
        TableName: tableName,
        Key: {
            Id: data['Items'][0].Id,
            userId: req.headers.userid
        },
        UpdateExpression: "SET #AssetType = :AssetType, #reminderType = :reminderType",
        ConditionExpression: "userId = :userId AND AssetId = :AssetId",
        ExpressionAttributeNames: { "#AssetType": "AssetType","#reminderType":"reminderType"},
        ExpressionAttributeValues: {
            ":userId": req.headers.userid,
            ":AssetId": req.body.id,
            ":AssetType": req.body.asset_type,
            ":reminderType": req.body.reminder_type
        },
        ReturnValues: "UPDATED_NEW",
    };
    result = await dynamoDB.updateDynamo(params);
    return result;

}


exports.deleteReminders = async (req, res) => {

    let data = await isItemExists(req);

    if (data.Items.length == 0) {
        let error = new Error();
        error = { message: "Item couldn't be found", statusCode: 2, status: 404 }
        logger.error(JSON.stringify(error));
    }

    const params = {
        TableName: tableName,
        Key: {
            Id: data['Items'][0].Id,
            userId: req.headers.userid
        },
        ConditionExpression: "userId = :userId AND AssetId = :AssetId AND AssetType = :AssetType",
        ExpressionAttributeValues: {
            ":userId": req.headers.userid,
            ":AssetId": req.body.id,
            ":AssetType": req.body.asset_type,
        },

    };


    result = await dynamoDB.deleteDynamo(params);
    return result;
}


isItemExists = async (req) => {
    const getParams = {
        TableName: tableName,
        IndexName: indexName,
        KeyConditionExpression: 'userId = :userId',
        FilterExpression: 'AssetId = :AssetId',
        ExpressionAttributeValues: {
            ':userId': req.headers.userid,
            ':AssetId': req.body.id ? req.body.id : req.query.id
        }
    };

    data = await dynamoDB.getDynamo(getParams);
    return data;

}

