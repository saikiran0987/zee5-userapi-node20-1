const requestObject = require('../../helpers/requestHelper');
// const solarDb = require('../../connections/solrConnection');
const unknown = 99;
const logger = require('../../helpers/logger')

function isNullOrEmpty(input) {
    return (input == null || input === "");
}


exports.verifyWatchlist = (req, res, next) => {
    let error = new Error();
    let body = req.body;
    let asset_type = req.body.hasOwnProperty('asset_type') ? req.body.asset_type : req.query.asset_type;
    let asset_id = req.body.id ? req.body.id : req.query.id;
    const regexp = new RegExp(`\\b-${asset_type}-\\b`);

    // Validate request object and its fields 
    if (requestObject.isEmpty(body) || isNullOrEmpty(asset_id) || isNullOrEmpty(asset_type) ||
        (isNullOrEmpty(req.body.duration) || req.body.duration <= 0) || isNullOrEmpty(req.body.date)) {

        error = { message: 'No item in request', statusCode: 3, status: 400 }
        logger.error(JSON.stringify(error))
    } 
    // validate asset type should not be unknown
    else if (asset_type == unknown) {
        error = { message: "Invalid asset type", statusCode: 3, status: 404 }
        logger.error(JSON.stringify(error))

    }
    // validate asset type is matching with given asset id
    else if (asset_id.match(regexp) == null) {
        error = {
            message: "Asset type is not matched with the specified asset id",
            statusCode: 3,
            status: 404
        }
        logger.error(JSON.stringify(error))
    }
    // validate given asset ID belongs to any asset or not
    // else if (validateAssetId(req, res, next) == 0){
    //     error = { message: "Item couldn't be found", statusCode: 2, status: 404 }
    //     logger.error(JSON.stringify(error));
    // }
    else {
        next();
    }
}

// this validation is not used in existing APP
//  async function validateAssetId(req,res,next)  {
//     try {
//         let params = {};
//         params["q"] = "id:" + req.id + "";
//         params["fl"] = ["id"];
//         params["start"] = 0;
//         params["rows"] = 1;
//           detailsResponse =  await solarDb.getSolrData(params);
//         return detailsResponse.length;

//     } catch (error) {
//         logger.error(JSON.stringify(error));
//     }

// };
