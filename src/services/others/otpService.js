const dynamoDB = require('../../connections/dynamoConnection');
const config = require('../../config/config');
const otptableName = config.onetimepassword.table;
const otpindexName = config.onetimepassword.index;
const UUID = require('uuid');

module.exports ={
    validateCode : async (req) => {
        try {
            const getParams = {
                TableName: otptableName,
                KeyConditionExpression: 'UserId = :userId',
                FilterExpression: 'Code = :code',
                ExpressionAttributeValues: {
                    ':code': req.body.code,
                    ':userId':req.headers.userid
                },
            };
            data = await dynamoDB.getDynamo(getParams);
            return data;
        } 
        catch (error) {
            console.error(error);
        }  
    },    
}

