let isObjectEmpty = (obj) => {
	if (obj && Object.keys(obj).length === 0 && obj.constructor === Object) {
		return true;
	} else {
		return false;
	}
};

/**
 * exporting functions.
 */
module.exports = {
	isEmpty: isObjectEmpty,
};
