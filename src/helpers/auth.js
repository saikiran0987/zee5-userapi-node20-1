
const crypto = require('crypto');
const utf8 = require('utf8');
const SIGNINGKEY= process.env.USERAPI_SIGNINGKEY;
const USERSECRET=process.env.USERAPI_USERSECRET;
const createAuthKeys =(bodyParams,path,b2bUserSecret=null)=>{
    let secret = b2bUserSecret?b2bUserSecret:USERSECRET;
   let finalKey = utf8.encode(SIGNINGKEY);
   let data = typeof bodyParams == 'object' ?`${JSON.stringify(bodyParams)}:${path}`:`:${path}`;
   let finalData = utf8.encode(data);
   let hmac =crypto.createHmac('sha256', finalKey).update(finalData).digest('base64');
   return `Z5 ${secret}:${hmac}`;
}





module.exports = {
    createAuthKeys :createAuthKeys,
}
