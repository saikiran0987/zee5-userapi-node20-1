const jwt = require('jsonwebtoken');
const appDir = require('./rootDirectory');
const fs = require('fs');
const auth = require("./auth");
const axiosHelper = require('./axiosCalls')

const verifyClaims =(token)=>{
    let tokenValue =  token.split(" ");  
    return new Promise((resolve,reject)=>{
        const secretKeyFile =fs.readFileSync(`${appDir}/keyos/rsa.pem`);
        jwt.verify(tokenValue[1], secretKeyFile, function (err, decoded) {
            if(err){
                reject({data:{"message":"Invalid token"},status:400})
            }else{
              
                resolve(decoded)
            }
        })
    })
}

const getJwtToken=(bodyParams)=>{
    let type = bodyParams.type;
    let bodyForAuthKey= {mobile:bodyParams.value};
    let path = `/v1/manage/customer/token?${type}=${(bodyParams.value).trim()}`;
    bodyForAuthKey="";
 
    let authKey = auth.createAuthKeys(bodyForAuthKey,path);
    return axiosHelper.call("GET",`${process.env.USER_API_BASE_URL}${path}`,"",{
        "Accept":"application/json",
        "Authorization":`${authKey}`,
      });
}


module.exports = {
    verifyToken :verifyClaims,
    getJwtToken:getJwtToken
  }