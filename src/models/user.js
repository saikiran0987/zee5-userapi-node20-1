const {dynamoDB} = require('../connections/awsConnection');
const logger = require('../helpers/logger');
const config = require('../config/config');
var userParams = {
  TableName:config.users.table,
  AttributeDefinitions: [{
          AttributeName: 'Id', //partition key
          AttributeType: 'S'
      },
      {
          AttributeName : 'Email',
          AttributeType : 'S'
      }
  ],
  KeySchema: [{
          AttributeName: 'Id',
          KeyType: 'HASH'
      }
  ],

  GlobalSecondaryIndexes:[
    {
        IndexName : config.users.email_index,
        KeySchema : [{
            AttributeName : 'Email',
            KeyType : 'HASH',
        }],
        Projection:{
            ProjectionType: 'ALL'
        },
        ProvisionedThroughput: {
            ReadCapacityUnits: 10,
            WriteCapacityUnits : 20,
        },
    },
  ],

  ProvisionedThroughput: {
      ReadCapacityUnits: 10,
      WriteCapacityUnits: 10
  },
}



dynamoDB.listTables({}, function(err, data) {
    let tables = null;
    if(data){
        tables= data.TableNames;
    }
    if(!data || tables.indexOf(userParams.TableName) === -1){

    dynamoDB.createTable(userParams, function (err, data) {
        console.log('tables');
      if (err) {
          console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
      } else {
          console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
      }
    });
  }
    else{
        // check for index if not exist update table for adding email index
    }
});