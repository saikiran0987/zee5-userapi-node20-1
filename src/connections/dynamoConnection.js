const awsObj = require("./awsConnection");
const logger = require('../helpers/logger');
module.exports = {
	find: _find = async (params) => {
		let start = process.hrtime();
		try {
			logger.info("Dynamo is hit")
			let resp = {};
			let result = new Promise(async (res, rej) => {
				awsObj.dynamo.query(params, function (err, data) {
					if (err) {
						logger.warn(`Warning in dynamo - ${err}`);
						res([]);
					}
					res(data);
				});
			});

			resp = _fetchData(result);
			logger.info("Dynamo process ended", start);
			return resp;
		} catch (error) {
			let errorObj = util.error("Data not found", "101");
			delete errorObj.http_code;
			logger.warn("Warning in dynamo wrapper", start, "info", error);
			return errorObj;
		}
	},
	getDynamo:_getDynamo = async (params) => {
		return awsObj.dynamo.query(params).promise()
	},
	getAllDynamo:_getAllDynamo = async (params) => {
		return awsObj.dynamo.scan(params).promise()
	},
	addDynamo: _addDynamo = async (params) => {
		return awsObj.dynamo.put(params).promise()
	},
	updateDynamo: _updateDynamo = async (params) => {
		return awsObj.dynamo.update(params).promise()
	},
	deleteDynamo: _deleteDynamo = async (params) => {
		return awsObj.dynamo.delete(params).promise()
	},
	get:_get = async (params) => {
		return awsObj.dynamo.get(params).promise()
	}
};
const _fetchData = async (promArr) => {
	let resp = {};
	let data = await Promise.race([
		util.timeout(process.env.DYNAMO_TIMEOUT),
		promArr,
	])
		.then((res) => {
			if (!res) {
				logger.error("Dynamo Timeout");
			}
			return res;
		})
		.catch((e) => {
			logger.error("Dynamo Timeout");
			promArr = null;
			return null;
		});
	if (data.hasOwnProperty("Items") && data.Items.length > 0) {
		if (data.Items[0].completed == true) {
			logger.error("Data not found");
		}
		resp = data.Items[0];
	} else {
		logger.error("Data not found");
	}
	return resp;
};