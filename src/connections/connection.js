const sql = require('mssql')

const sqlConfig = {
    user:process.env.DB_USER,
    password: process.env.DB_PWD,
    database: process.env.DB_NAME,
    server: process.env.DB_HOST,
    //  driver: 'tedious', // required when use sequlize.
    port:+process.env.DB_PORT,
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    },
    options: {
        encrypt: false, // for azure
        trustServerCertificate: false,// change to true for local dev / self-signed certs
        trustedConnection: false
    }
}
module.exports = sqlConfig;
