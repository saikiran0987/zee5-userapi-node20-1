const SecretsManager  = require('../connections/awsConnection').SecretsManager;
const { SecretsManagerCache } = require("aws-secrets-manager-cache");
const helper = new SecretsManagerCache({ secretsManager: SecretsManager, ttl: 300000 });

function getSecrets(secretName) {
    return new Promise(async function (resolve, reject) {
        try {
            const secretJson = await helper.getSecret(secretName, true);
            return resolve(secretJson);
        }
        catch (error) {
            return reject(error);
        }
    });
}


module.exports = getSecrets;
