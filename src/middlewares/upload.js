const multer = require('multer');
const util = require("util");
var fs = require('fs');
var dir = './public';

if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}
const fileStorageEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, dir); //important this is a direct path fron our current file to storage location
  },
  filename: (req, file, cb) => {
    cb(null,  file.originalname);
  },
});

const upload = multer({ storage: fileStorageEngine }).single('file');

let uploadFileMiddleware = util.promisify(upload);
module.exports = uploadFileMiddleware;