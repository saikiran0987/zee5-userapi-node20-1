const ErrorStackParser = require('error-stack-parser');
const logger = require('../middlewares/winston')

module.exports = {
    error: function (message = 'ERROR', code = 400, httpCode = 400, meta = {}) {
        let status = {
            error_code: `${code}`,
            error_msg: message,
            http_code: httpCode,
            assetDetails: meta
        };
        if (_.isEmpty(meta)) delete (status.assetDetails);
        return status;
    },
    timeout: function (ms) {
        return new Promise(reject => setTimeout(reject, ms));
    },
    log: function (message = "", level = "info", errorObj = "") {
        let duration = false;
        if(errorObj.hasOwnProperty("startTime")){
            duration= (Date.now() - errorObj.startTime) / 1000
            console.log((Date.now() - errorObj.startTime) / 1000)
        }
        if(level === 'error') {
            if (errorObj.hasOwnProperty("stack")) {
                errorObj = ErrorStackParser.parse(errorObj)[0];
                message = JSON.stringify({message: message, ...errorObj})
            }
        }
        else {
            if (errorObj) {
                message = JSON.stringify({message: message, ...errorObj})
            }
        }
        if(duration){
            logger.log({
                level: level,
                duration: duration,
                message: message
            });
        }
        else {
            logger.log({
                level: level,
                duration: 0,
                message: message
            });
        }
        logger.profile('testing')
        logger.profile('testing')

    },
}