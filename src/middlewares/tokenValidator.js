const authService = require("../services/authService");
const _ = require("lodash");
const logger = require('../helpers/logger')
async function tokenValidator(request, response, next) {
 try {
    const body = request.body;
    if (!body["Authorization"] && !body["X-Z5-Guest-Token"]) {
        logger.error(`Access token not available in body`);
        return response.status(401).send({ error_code: "401", error_msg: "Token not found" });
    }
    if (body["Authorization"] && body["X-Z5-Guest-Token"]) { //Decide error code and message 
        logger.error(`Both guest and auth token present in body`);
        return response.status(401).send({ error_code: "401", error_msg: "Invalid Token" });
    }       
 
    let decoded;
    if (body.hasOwnProperty("Authorization") && !_.isEmpty(body["Authorization"])) {
        if (body["Authorization"].indexOf("bearer") != -1) {
            let n = body["Authorization"].indexOf("bearer");
            body["Authorization"] = body["Authorization"].slice(n + 7);
        }
            decoded = await authService.verifyToken(body["Authorization"]);
    }
    else if (body.hasOwnProperty("X-Z5-Guest-Token") && !_.isEmpty(body["X-Z5-Guest-Token"])) {
        if(body["X-Z5-Guest-Token"].length < 32){
        return response.status(401).send({ error_code: "401", error_msg: "Token Invalid" });
        }
    }
    if (_.isEmpty(body["X-Z5-Guest-Token"])) {
        if (decoded != null && decoded != undefined) {
            if (Object.keys(decoded).includes('error')) {
                let errorMsg;
                if (decoded["error"]["message"] == 'jwt malformed' || decoded["error"]["message"] == 'invalid token') {
                    errorMsg = 'Token Invalid';
                }
                else {
                    errorMsg = 'Token Expired';
                }
                return response.status(401).send({ error_code: "401", error_msg: errorMsg });
            }
        }
        else {
            return response.status(401).send({ error_code: "401", error_msg: "Token Invalid" });
        }
    }

    if (!_.isEmpty(body["x-access-token"])) {
        try {
            await jwt.verify(body["x-access-token"], "testsecret");
        } catch (error) {
            logger.error(JSON.stringify(error))
            logger.error(`Access token invalid in body`);
            return response.status(400).send({ error_code: "401", error_msg: "x-access-token is not valid" });
        }
    }
    next();
    return decoded;
    }
    catch (error) {
        return response.send({
        errorCode: 400,
        errorMessage: error
        })
    }

};
 
module.exports = tokenValidator;