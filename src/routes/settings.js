const express = require('express');
const router = express.Router();
const settingsController = require('../controllers/settingsController');
const {addSettings,updateSettingsData,deleteSettingsData} = require('../services/settings/settingsValidation');

const { body } = require('express-validator');
//import validation file functions

// get pserson
router.get('/', settingsController.getSettings);

// add person
router.post('/',addSettings,settingsController.addSettings);

// update person
router.put('/',updateSettingsData,settingsController.updateSettings);

// update person
router.delete('/',deleteSettingsData,settingsController.deleteSettings);

//global export
module.exports = router;