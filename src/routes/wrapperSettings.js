const router = require("express").Router();
const wrapperController = require("../controllers/wrapperSettingsController");
const devceValidator = require('../middlewares/deviceValidator');

//v1/settings
router.get('/', devceValidator, wrapperController.getSettings);
router.post('/', devceValidator, wrapperController.addSettings);
router.put('/', devceValidator, wrapperController.updateSettings);
router.delete('/', devceValidator, wrapperController.deleteSettings);

// export router.
module.exports = router;