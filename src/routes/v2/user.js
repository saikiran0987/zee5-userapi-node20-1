const {authenticateToken} = require("../../services/authService");
const express = require("express");
const router = express.Router();
const userController = require("../../controllers/userController");
const { updateUser , verifyConfirmationKey,getUserWithEmail } = require("../../services/user/userValidation");



// confirmemail
router.put('/confirmemail',verifyConfirmationKey,userController.confirmemailv2);


// update person
router.put('/', userController.UpdateUserV2);


//login email
router.post('/',userController.LoginEmailV2);


module.exports = router;