const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const { verifyConfirmationKey, changePasswordValidator } = require("../services/user/userValidation");

const { getUser } = require("../services/user/userValidation");
// get user
router.get('/', getUser, userController.getUser);
// update user
router.put('/', userController.updateUser);
//delete user
router.delete('/', userController.deleteUser);
// confirmmobile
router.put('/confirmmobile',verifyConfirmationKey,userController.confirmMobile);
//changepassword
router.put('/changePassword', changePasswordValidator, userController.changePassword);
// export router.
module.exports = router;