const express = require("express");
const router = express.Router();
const wrapperController = require("../controllers/wrapperController");
const devceValidator = require('../middlewares/deviceValidator');

// Please change routing method by self
router.post("/loginmobile",devceValidator, wrapperController.loginMobile);
router.post('/loginemail',devceValidator, wrapperController.loginEmail);
// export router.
module.exports = router;
