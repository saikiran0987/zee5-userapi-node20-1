
const express = require('express');
const router = express.Router();
const filesController = require('../controllers/filesController');


// upload  file to public folder
router.post('/upload', filesController.uploaDumpFile);

//seed data to given table 
router.post('/seed', filesController.seedData);

// get list of files
router.get('/', filesController.getListFiles);



module.exports = router ;